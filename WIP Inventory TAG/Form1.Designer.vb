﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.Btt_About = New System.Windows.Forms.Button
        Me.BottomToolStripPanel = New System.Windows.Forms.ToolStripPanel
        Me.TopToolStripPanel = New System.Windows.Forms.ToolStripPanel
        Me.RightToolStripPanel = New System.Windows.Forms.ToolStripPanel
        Me.LeftToolStripPanel = New System.Windows.Forms.ToolStripPanel
        Me.ContentPanel = New System.Windows.Forms.ToolStripContentPanel
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Btt_Prt_List = New System.Windows.Forms.Button
        Me.Btt_Help = New System.Windows.Forms.Button
        Me.Btt_Chk_Diff = New System.Windows.Forms.Button
        Me.Btt_Prt_Tag = New System.Windows.Forms.Button
        Me.Btt_Stk_List = New System.Windows.Forms.Button
        Me.Btt_Upload_Act = New System.Windows.Forms.Button
        Me.DG_View = New System.Windows.Forms.DataGridView
        Me.faccd = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.itemcd = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.itemdesc = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.stocklocation = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lotno = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.tagid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.onhand = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.actualcount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.diffqty = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.unitdesc = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.Lb_Status = New System.Windows.Forms.Label
        Me.Lb_Rec_Count = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Cmb_Line = New System.Windows.Forms.ComboBox
        Me.Btt_Update = New System.Windows.Forms.Button
        Me.Btt_Export = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.Txb_Save_To = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Cmb_factory = New System.Windows.Forms.ComboBox
        Me.Button1 = New System.Windows.Forms.Button
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog
        Me.OpenFileDialog = New System.Windows.Forms.OpenFileDialog
        Me.Panel1.SuspendLayout()
        CType(Me.DG_View, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Btt_About
        '
        Me.Btt_About.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Btt_About.Image = CType(resources.GetObject("Btt_About.Image"), System.Drawing.Image)
        Me.Btt_About.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Btt_About.Location = New System.Drawing.Point(1, 425)
        Me.Btt_About.Margin = New System.Windows.Forms.Padding(4)
        Me.Btt_About.Name = "Btt_About"
        Me.Btt_About.Size = New System.Drawing.Size(122, 49)
        Me.Btt_About.TabIndex = 6
        Me.Btt_About.Text = "About"
        Me.Btt_About.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btt_About.UseVisualStyleBackColor = True
        '
        'BottomToolStripPanel
        '
        Me.BottomToolStripPanel.Location = New System.Drawing.Point(0, 0)
        Me.BottomToolStripPanel.Name = "BottomToolStripPanel"
        Me.BottomToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me.BottomToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.BottomToolStripPanel.Size = New System.Drawing.Size(0, 0)
        '
        'TopToolStripPanel
        '
        Me.TopToolStripPanel.Location = New System.Drawing.Point(0, 0)
        Me.TopToolStripPanel.Name = "TopToolStripPanel"
        Me.TopToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me.TopToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.TopToolStripPanel.Size = New System.Drawing.Size(0, 0)
        '
        'RightToolStripPanel
        '
        Me.RightToolStripPanel.Location = New System.Drawing.Point(0, 0)
        Me.RightToolStripPanel.Name = "RightToolStripPanel"
        Me.RightToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me.RightToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.RightToolStripPanel.Size = New System.Drawing.Size(0, 0)
        '
        'LeftToolStripPanel
        '
        Me.LeftToolStripPanel.Location = New System.Drawing.Point(0, 0)
        Me.LeftToolStripPanel.Name = "LeftToolStripPanel"
        Me.LeftToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me.LeftToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.LeftToolStripPanel.Size = New System.Drawing.Size(0, 0)
        '
        'ContentPanel
        '
        Me.ContentPanel.Size = New System.Drawing.Size(150, 150)
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel1.Controls.Add(Me.Btt_Prt_List)
        Me.Panel1.Controls.Add(Me.Btt_Help)
        Me.Panel1.Controls.Add(Me.Btt_Chk_Diff)
        Me.Panel1.Controls.Add(Me.Btt_Prt_Tag)
        Me.Panel1.Controls.Add(Me.Btt_About)
        Me.Panel1.Controls.Add(Me.Btt_Stk_List)
        Me.Panel1.Location = New System.Drawing.Point(3, 62)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(129, 483)
        Me.Panel1.TabIndex = 1
        '
        'Btt_Prt_List
        '
        Me.Btt_Prt_List.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Btt_Prt_List.Image = CType(resources.GetObject("Btt_Prt_List.Image"), System.Drawing.Image)
        Me.Btt_Prt_List.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Btt_Prt_List.Location = New System.Drawing.Point(1, 77)
        Me.Btt_Prt_List.Margin = New System.Windows.Forms.Padding(4)
        Me.Btt_Prt_List.Name = "Btt_Prt_List"
        Me.Btt_Prt_List.Size = New System.Drawing.Size(122, 74)
        Me.Btt_Prt_List.TabIndex = 4
        Me.Btt_Prt_List.Text = "      Print" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "List"
        Me.Btt_Prt_List.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btt_Prt_List.UseVisualStyleBackColor = True
        '
        'Btt_Help
        '
        Me.Btt_Help.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Btt_Help.Image = CType(resources.GetObject("Btt_Help.Image"), System.Drawing.Image)
        Me.Btt_Help.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Btt_Help.Location = New System.Drawing.Point(1, 376)
        Me.Btt_Help.Margin = New System.Windows.Forms.Padding(4)
        Me.Btt_Help.Name = "Btt_Help"
        Me.Btt_Help.Size = New System.Drawing.Size(122, 48)
        Me.Btt_Help.TabIndex = 5
        Me.Btt_Help.Text = "Help"
        Me.Btt_Help.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btt_Help.UseVisualStyleBackColor = True
        '
        'Btt_Chk_Diff
        '
        Me.Btt_Chk_Diff.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Btt_Chk_Diff.Image = CType(resources.GetObject("Btt_Chk_Diff.Image"), System.Drawing.Image)
        Me.Btt_Chk_Diff.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Btt_Chk_Diff.Location = New System.Drawing.Point(1, 299)
        Me.Btt_Chk_Diff.Margin = New System.Windows.Forms.Padding(4)
        Me.Btt_Chk_Diff.Name = "Btt_Chk_Diff"
        Me.Btt_Chk_Diff.Size = New System.Drawing.Size(122, 74)
        Me.Btt_Chk_Diff.TabIndex = 4
        Me.Btt_Chk_Diff.Text = "      Check DIFF"
        Me.Btt_Chk_Diff.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btt_Chk_Diff.UseVisualStyleBackColor = True
        '
        'Btt_Prt_Tag
        '
        Me.Btt_Prt_Tag.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Btt_Prt_Tag.Image = CType(resources.GetObject("Btt_Prt_Tag.Image"), System.Drawing.Image)
        Me.Btt_Prt_Tag.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Btt_Prt_Tag.Location = New System.Drawing.Point(1, 151)
        Me.Btt_Prt_Tag.Margin = New System.Windows.Forms.Padding(4)
        Me.Btt_Prt_Tag.Name = "Btt_Prt_Tag"
        Me.Btt_Prt_Tag.Size = New System.Drawing.Size(122, 74)
        Me.Btt_Prt_Tag.TabIndex = 5
        Me.Btt_Prt_Tag.Text = "      Print TAG"
        Me.Btt_Prt_Tag.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btt_Prt_Tag.UseVisualStyleBackColor = True
        '
        'Btt_Stk_List
        '
        Me.Btt_Stk_List.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Btt_Stk_List.Image = CType(resources.GetObject("Btt_Stk_List.Image"), System.Drawing.Image)
        Me.Btt_Stk_List.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Btt_Stk_List.Location = New System.Drawing.Point(1, 3)
        Me.Btt_Stk_List.Margin = New System.Windows.Forms.Padding(4)
        Me.Btt_Stk_List.Name = "Btt_Stk_List"
        Me.Btt_Stk_List.Size = New System.Drawing.Size(122, 74)
        Me.Btt_Stk_List.TabIndex = 3
        Me.Btt_Stk_List.Text = "      Stock List"
        Me.Btt_Stk_List.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btt_Stk_List.UseVisualStyleBackColor = True
        '
        'Btt_Upload_Act
        '
        Me.Btt_Upload_Act.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Btt_Upload_Act.Image = CType(resources.GetObject("Btt_Upload_Act.Image"), System.Drawing.Image)
        Me.Btt_Upload_Act.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Btt_Upload_Act.Location = New System.Drawing.Point(6, 289)
        Me.Btt_Upload_Act.Margin = New System.Windows.Forms.Padding(4)
        Me.Btt_Upload_Act.Name = "Btt_Upload_Act"
        Me.Btt_Upload_Act.Size = New System.Drawing.Size(122, 74)
        Me.Btt_Upload_Act.TabIndex = 3
        Me.Btt_Upload_Act.Text = "      Upload Actual"
        Me.Btt_Upload_Act.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btt_Upload_Act.UseVisualStyleBackColor = True
        '
        'DG_View
        '
        Me.DG_View.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.DG_View.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DG_View.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.faccd, Me.itemcd, Me.itemdesc, Me.stocklocation, Me.lotno, Me.tagid, Me.onhand, Me.actualcount, Me.diffqty, Me.unitdesc})
        Me.DG_View.Location = New System.Drawing.Point(149, 62)
        Me.DG_View.Name = "DG_View"
        Me.DG_View.RowHeadersVisible = False
        Me.DG_View.Size = New System.Drawing.Size(778, 432)
        Me.DG_View.TabIndex = 2
        '
        'faccd
        '
        Me.faccd.DataPropertyName = "faccd"
        Me.faccd.HeaderText = "Fac Cd"
        Me.faccd.Name = "faccd"
        Me.faccd.Width = 76
        '
        'itemcd
        '
        Me.itemcd.DataPropertyName = "itemcd"
        Me.itemcd.HeaderText = "Item Cd"
        Me.itemcd.Name = "itemcd"
        Me.itemcd.Width = 78
        '
        'itemdesc
        '
        Me.itemdesc.DataPropertyName = "itemdesc"
        Me.itemdesc.HeaderText = "Item Desc"
        Me.itemdesc.Name = "itemdesc"
        Me.itemdesc.Width = 93
        '
        'stocklocation
        '
        Me.stocklocation.DataPropertyName = "stocklocation"
        Me.stocklocation.HeaderText = "Location"
        Me.stocklocation.Name = "stocklocation"
        Me.stocklocation.Width = 84
        '
        'lotno
        '
        Me.lotno.DataPropertyName = "lotno"
        Me.lotno.HeaderText = "Lot No"
        Me.lotno.Name = "lotno"
        Me.lotno.Width = 72
        '
        'tagid
        '
        Me.tagid.DataPropertyName = "tagid"
        Me.tagid.HeaderText = "TAG No"
        Me.tagid.Name = "tagid"
        Me.tagid.Visible = False
        Me.tagid.Width = 82
        '
        'onhand
        '
        Me.onhand.DataPropertyName = "onhand"
        Me.onhand.HeaderText = "OH Qty"
        Me.onhand.Name = "onhand"
        Me.onhand.Width = 76
        '
        'actualcount
        '
        Me.actualcount.DataPropertyName = "actualcount"
        Me.actualcount.HeaderText = "Act Qty"
        Me.actualcount.Name = "actualcount"
        Me.actualcount.Width = 75
        '
        'diffqty
        '
        Me.diffqty.DataPropertyName = "diffqty"
        Me.diffqty.HeaderText = "Diff Qty"
        Me.diffqty.Name = "diffqty"
        Me.diffqty.Width = 75
        '
        'unitdesc
        '
        Me.unitdesc.DataPropertyName = "unitdesc"
        Me.unitdesc.HeaderText = "Unit Desc"
        Me.unitdesc.Name = "unitdesc"
        Me.unitdesc.Width = 91
        '
        'Panel2
        '
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel2.Controls.Add(Me.Lb_Status)
        Me.Panel2.Controls.Add(Me.Lb_Rec_Count)
        Me.Panel2.Location = New System.Drawing.Point(482, 512)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(445, 33)
        Me.Panel2.TabIndex = 3
        '
        'Lb_Status
        '
        Me.Lb_Status.BackColor = System.Drawing.Color.GreenYellow
        Me.Lb_Status.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Lb_Status.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Lb_Status.Location = New System.Drawing.Point(3, 2)
        Me.Lb_Status.Name = "Lb_Status"
        Me.Lb_Status.Size = New System.Drawing.Size(280, 25)
        Me.Lb_Status.TabIndex = 1
        Me.Lb_Status.Text = "Status : Ready"
        Me.Lb_Status.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Lb_Rec_Count
        '
        Me.Lb_Rec_Count.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Lb_Rec_Count.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Lb_Rec_Count.Location = New System.Drawing.Point(289, 2)
        Me.Lb_Rec_Count.Name = "Lb_Rec_Count"
        Me.Lb_Rec_Count.Size = New System.Drawing.Size(149, 25)
        Me.Lb_Rec_Count.TabIndex = 0
        Me.Lb_Rec_Count.Text = "Total 0 Records"
        Me.Lb_Rec_Count.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Navy
        Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label2.Font = New System.Drawing.Font("Monotype Corsiva", 26.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.LemonChiffon
        Me.Label2.Location = New System.Drawing.Point(12, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(300, 45)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "WIP Inventory TAG"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label3.Location = New System.Drawing.Point(748, 34)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(70, 20)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Location"
        '
        'Cmb_Line
        '
        Me.Cmb_Line.FormattingEnabled = True
        Me.Cmb_Line.Location = New System.Drawing.Point(842, 32)
        Me.Cmb_Line.Name = "Cmb_Line"
        Me.Cmb_Line.Size = New System.Drawing.Size(80, 24)
        Me.Cmb_Line.TabIndex = 2
        Me.Cmb_Line.Text = "Choose"
        '
        'Btt_Update
        '
        Me.Btt_Update.Enabled = False
        Me.Btt_Update.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Btt_Update.Image = CType(resources.GetObject("Btt_Update.Image"), System.Drawing.Image)
        Me.Btt_Update.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Btt_Update.Location = New System.Drawing.Point(149, 500)
        Me.Btt_Update.Name = "Btt_Update"
        Me.Btt_Update.Size = New System.Drawing.Size(140, 45)
        Me.Btt_Update.TabIndex = 7
        Me.Btt_Update.Text = "Update"
        Me.Btt_Update.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btt_Update.UseVisualStyleBackColor = True
        '
        'Btt_Export
        '
        Me.Btt_Export.Enabled = False
        Me.Btt_Export.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Btt_Export.Image = CType(resources.GetObject("Btt_Export.Image"), System.Drawing.Image)
        Me.Btt_Export.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Btt_Export.Location = New System.Drawing.Point(290, 500)
        Me.Btt_Export.Name = "Btt_Export"
        Me.Btt_Export.Size = New System.Drawing.Size(140, 45)
        Me.Btt_Export.TabIndex = 8
        Me.Btt_Export.Text = "Export"
        Me.Btt_Export.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btt_Export.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(318, 35)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(63, 16)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "Save to : "
        '
        'Txb_Save_To
        '
        Me.Txb_Save_To.Location = New System.Drawing.Point(387, 32)
        Me.Txb_Save_To.Name = "Txb_Save_To"
        Me.Txb_Save_To.Size = New System.Drawing.Size(247, 22)
        Me.Txb_Save_To.TabIndex = 10
        Me.Txb_Save_To.Text = "D:\WIP"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label4.Location = New System.Drawing.Point(748, 5)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(78, 20)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "Fac Code"
        '
        'Cmb_factory
        '
        Me.Cmb_factory.FormattingEnabled = True
        Me.Cmb_factory.Location = New System.Drawing.Point(842, 3)
        Me.Cmb_factory.Name = "Cmb_factory"
        Me.Cmb_factory.Size = New System.Drawing.Size(80, 24)
        Me.Cmb_factory.TabIndex = 1
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(638, 31)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 13
        Me.Button1.Text = "Browse"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(939, 550)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Cmb_factory)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Btt_Upload_Act)
        Me.Controls.Add(Me.Txb_Save_To)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Btt_Export)
        Me.Controls.Add(Me.Btt_Update)
        Me.Controls.Add(Me.Cmb_Line)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.DG_View)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "WIP Inventory TAG V.3.0.1"
        Me.Panel1.ResumeLayout(False)
        CType(Me.DG_View, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Btt_About As System.Windows.Forms.Button
    Friend WithEvents BottomToolStripPanel As System.Windows.Forms.ToolStripPanel
    Friend WithEvents TopToolStripPanel As System.Windows.Forms.ToolStripPanel
    Friend WithEvents RightToolStripPanel As System.Windows.Forms.ToolStripPanel
    Friend WithEvents LeftToolStripPanel As System.Windows.Forms.ToolStripPanel
    Friend WithEvents ContentPanel As System.Windows.Forms.ToolStripContentPanel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Btt_Prt_Tag As System.Windows.Forms.Button
    Friend WithEvents Btt_Stk_List As System.Windows.Forms.Button
    Friend WithEvents DG_View As System.Windows.Forms.DataGridView
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Lb_Rec_Count As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Cmb_Line As System.Windows.Forms.ComboBox
    Friend WithEvents Lb_Status As System.Windows.Forms.Label
    Friend WithEvents Btt_Chk_Diff As System.Windows.Forms.Button
    Friend WithEvents Btt_Upload_Act As System.Windows.Forms.Button
    Friend WithEvents Btt_Update As System.Windows.Forms.Button
    Friend WithEvents Btt_Export As System.Windows.Forms.Button
    Friend WithEvents Btt_Help As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Btt_Prt_List As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Cmb_factory As System.Windows.Forms.ComboBox
    Friend WithEvents faccd As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents itemcd As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents itemdesc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents stocklocation As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lotno As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tagid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents onhand As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents actualcount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents diffqty As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents unitdesc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Txb_Save_To As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents OpenFileDialog As System.Windows.Forms.OpenFileDialog

End Class
