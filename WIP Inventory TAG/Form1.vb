﻿

Imports System.Data
Imports System.IO
Imports WIP_Inventory_TAG.wipTAG


Public Class Form1
    Public button_selection As String
    Public stk_month As String
    Public blk_tag As Integer



    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim ds_fac As New wipTAGDataSet
        Dim dt_fac As New wipTAGDataSet.wip_inventory_factoryDataTable
        Dim ad_fac As New wipTAGDataSetTableAdapters.wip_inventory_factoryTableAdapter

        Btt_Export.Enabled = False
        Btt_Update.Enabled = False
        Btt_Upload_Act.Enabled = False
        Btt_Chk_Diff.Enabled = False
        Btt_Help.Enabled = False

        Dim oldCI As System.Globalization.CultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture
        System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("en-US")

        stk_month = Format(Year(Now()), "0000") & Format(Month(Now()), "00")

        dt_fac = ds_fac.wip_inventory_factory

        ad_fac.Fill(dt_fac)

        With Cmb_factory

            .DataSource = dt_fac
            .DisplayMember = "fac_name"
            .ValueMember = "fac_code"
            .Text = "--Select--"
        End With

        Dim ds As New wipTAGDataSet
        Dim location_dt As New wipTAGDataSet.wip_inventory_locationDataTable
        Dim adapt As New wipTAGDataSetTableAdapters.wip_inventory_locationTableAdapter
        Dim faccode As String = Cmb_factory.Text.ToString

        location_dt = ds.wip_inventory_location

        adapt.FillBy(location_dt, faccode)

        With Cmb_Line

            .DataSource = location_dt
            .DisplayMember = "loc_name"
            .ValueMember = "loc_code"
            .Text = "ALL"
        End With

        GridViewHeaderText()
    End Sub

    Public Sub GridViewHeaderText()
        DG_View.Rows.Clear()
        DG_View.DataSource = Nothing

    End Sub

    Private Sub Btt_Stk_List_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btt_Stk_List.Click

        Lb_Status.Text = "Status : Get List"
        Lb_Status.BackColor = Color.Red
        DG_View.DataSource = ""
        DG_View.AutoGenerateColumns = False
        Lb_Rec_Count.Text = "Total 0 Records"

        Dim wiptagcls As New wipTAG

        Dim ds As New wipTAGDataSet
        Dim dt_stock As wipTAGDataSet.wip_inventory_stockDataTable
        Dim adapt As New wipTAGDataSetTableAdapters.wip_inventory_stockTableAdapter

        Dim faccode As String = Cmb_factory.Text.ToString
        Dim loccode As String = Cmb_Line.Text.ToString
        If loccode = "ALL" Then
            loccode = "%"
        End If
        'MsgBox(loccode, MsgBoxStyle.OkOnly, "List")

        
        stk_month = Format(Year(Now()), "0000") & Format(Month(Now()), "00")

        dt_stock = ds.wip_inventory_stock

        
        adapt.FillByFac(dt_stock, stk_month, faccode, loccode)

        Btt_Export.Enabled = False
        Btt_Update.Enabled = False
        button_selection = "STK_List"
        
        Try
            
            If Cmb_Line.Text = "" Then
                MsgBox("กรุณาเลือก Location ที่ต้องการก่อน", MsgBoxStyle.OkOnly, "Error Information")
                Lb_Status.Text = "Status : Stopped"
                Lb_Status.BackColor = Color.Red
                Exit Try
            Else
                Lb_Status.Text = "Status : Processing..."
                Lb_Status.BackColor = Color.Red

                '== Get Stock List =='
                DG_View.DataSource = dt_stock.DataSet.Tables(2)
               
            End If
            'Cmb_Line.ResetText()
            Lb_Status.Text = "Status : Finished"
            Lb_Status.BackColor = Color.LightGreen
            Lb_Rec_Count.Text = "Total " & (dt_stock.Rows.Count) & " Records"
        Catch ex As Exception
            MsgBox(ex.ToString)
            Cmb_Line.ResetText()
            Lb_Status.Text = "Status : Stopped"
            Lb_Status.BackColor = Color.Red
        End Try
    End Sub

    Private Sub Btt_Prt_List_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btt_Prt_List.Click
        Lb_Status.Text = "Status : Print List"
        Lb_Status.BackColor = Color.Red
        DG_View.DataSource = ""
        Lb_Rec_Count.Text = "Total 0 Record"

        Dim wiptagcls As New wipTAG

        Dim ds As New wipTAGDataSet
        Dim dt_stock As wipTAGDataSet.wip_inventory_stockDataTable
        Dim adapt As New wipTAGDataSetTableAdapters.wip_inventory_stockTableAdapter

        Dim faccode As String = Cmb_factory.Text.ToString
        Dim loccode As String = Cmb_Line.Text.ToString

        stk_month = Format(Year(Now()), "0000") & Format(Month(Now()), "00")

        dt_stock = ds.wip_inventory_stock


        adapt.FillByFac(dt_stock, stk_month, faccode, "%")

        Btt_Export.Enabled = False
        Btt_Update.Enabled = False
        button_selection = "Prt_List"

        stk_month = Format(Year(Now()), "0000") & Format(Month(Now()), "00")

        Try

            '== Get List Data ==='

            Lb_Status.Text = "Status : Processing..."
            Lb_Status.BackColor = Color.Red
            wiptagcls.Excel_Get_list_before(faccode, loccode, dt_stock)

            Lb_Status.Text = "Status : Finished"
            Lb_Status.BackColor = Color.LightGreen
            'Lb_Rec_Count.Text = "Total " & dt_stock.Rows.Count & " Records"
            Lb_Rec_Count.Text = "Total 0 Record"


        Catch ex As Exception
            MsgBox(ex.ToString)
            'Cmb_Line.ResetText()
            Lb_Status.Text = "Status : Stopped"
            Lb_Status.BackColor = Color.Red
        End Try
    End Sub

    Private Sub Btt_Prt_Tag_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btt_Prt_Tag.Click
        Lb_Status.Text = "Status : Print TAG"
        Lb_Status.BackColor = Color.Red
        DG_View.DataSource = ""
        Lb_Rec_Count.Text = "Total 0 Record"

        Dim wiptagcls As New wipTAG

        Dim ds As New wipTAGDataSet
        Dim dt_stock As New wipTAGDataSet.wip_inventory_stockDataTable
        Dim dt_tag As New wipTAGDataSet.wip_inventory_stockDataTable
        Dim adapt As New wipTAGDataSetTableAdapters.wip_inventory_stockTableAdapter

        Dim faccode As String = Cmb_factory.Text.ToString
        Dim loccode As String = Cmb_Line.Text.ToString

        stk_month = Format(Year(Now()), "0000") & Format(Month(Now()), "00")

        
        dt_stock = ds.wip_inventory_stock
        dt_tag = ds.wip_inventory_stock

        adapt.FillByFac(dt_stock, stk_month, faccode, "%")
        adapt.FillByFac(dt_tag, stk_month, faccode, "%")

        Btt_Export.Enabled = False
        Btt_Update.Enabled = False
        button_selection = "Prt_TAG"
        
        stk_month = Format(Year(Now()), "0000") & Format(Month(Now()), "00")

        Try
            
            '== Get Tag Data ==='
            Lb_Status.Text = "Status : Processing..."
            Lb_Status.BackColor = Color.Red

            wiptagcls.Excel_Get_list_after(faccode, loccode, dt_stock)
            'wiptagcls.Excel_Get_Tag(faccode, loccode, dt_tag)
            
            Lb_Status.Text = "Status : Finished"
            Lb_Status.BackColor = Color.LightGreen
            'Lb_Rec_Count.Text = "Total " & dt_stock.Rows.Count & " Records"
            Lb_Rec_Count.Text = "Total 0 Record"
        Catch ex As Exception
            MsgBox(ex.ToString)
            'Cmb_Line.ResetText()
            Lb_Status.Text = "Status : Stopped"
            Lb_Status.BackColor = Color.Red
        End Try
    End Sub

    Private Sub releaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub

    Private Sub Btt_Help_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btt_Help.Click
        button_selection = "Help"
        MsgBox("Please wait", MsgBoxStyle.OkOnly, "Help Information")
    End Sub

    Private Sub Btt_About_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btt_About.Click
        button_selection = "About"
        About.Show()
    End Sub

    Private Sub Btt_Upload_Act_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btt_Upload_Act.Click
        Dim wiptagcls As New wipTAG
        Dim file_select As String

        Btt_Export.Enabled = False
        Btt_Update.Enabled = False
        button_selection = "Upload_Act"
        DG_View.DataSource = ""
        Lb_Status.Text = "Update Actual Count"
        Lb_Status.BackColor = Color.Tomato
        Lb_Status.Refresh()
        Lb_Rec_Count.Text = "Total 0 Records"
        stk_month = Format(Year(Now()), "0000") & Format(Month(Now()), "00")

        Try
            Lb_Status.Text = "Upload Actual Count Excel file to Database"
            Lb_Status.BackColor = Color.LightPink
            Lb_Status.Refresh()
            OpenFileDialog.Filter = "|*.xlsx"
            OpenFileDialog.Title = "Upload Actual Count file"
            If OpenFileDialog.ShowDialog() <> Windows.Forms.DialogResult.Cancel Then
                file_select = OpenFileDialog.FileName
                wiptagcls.Clear_DB()
                wiptagcls.Get_Upload_Excel(file_select)
                Btt_Update.Enabled = True
            End If

            Cmb_Line.ResetText()
            Lb_Status.Text = "Ready"
            Lb_Status.BackColor = Color.YellowGreen
            Lb_Status.Refresh()
            Lb_Rec_Count.Text = "Total " & DG_View.Rows.Count - 1 & " Records"
            Lb_Rec_Count.Refresh()
        Catch ex As Exception
            MsgBox(ex.ToString)
            Cmb_Line.ResetText()
            Lb_Status.Text = "Ready"
            Lb_Status.BackColor = Color.YellowGreen
            Lb_Status.Refresh()
        End Try
    End Sub

    Private Sub Btt_Chk_Diff_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btt_Chk_Diff.Click
        'Dim diff_dt As wipTAGDataSet.wip_inventory_tagDataTable
        'Dim diff_dt As wipTAGDataSet.diff_tableDataTable
        Dim diff_view As New DataView
        Dim wiptagcls As New wipTAG

        Btt_Export.Enabled = False
        Btt_Update.Enabled = False
        button_selection = "Check_Diff"
        DG_View.DataSource = ""
        Lb_Rec_Count.Text = "Total 0 Records"
        stk_month = Format(Year(Now()), "0000") & Format(Month(Now()), "00")

        Try
            Lb_Status.Text = "Compare Stock Data wiht Actual Count"
            Lb_Status.BackColor = Color.Tomato
            Lb_Status.Refresh()
            '======= Get Diff Data =========='
            'diff_dt = wiptagcls.Get_Diff_Data()
            'If diff_dt.Rows.Count > 0 Then
            'DG_View.AutoGenerateColumns = False
            'DG_View.Columns("wos").Visible = False
            'DG_View.DataSource = diff_dt
            'Btt_Export.Enabled = True
            'MsgBox(diff_dt.Count)
            'wiptagcls.Excel_Get_Diff(diff_dt, "Diff")
            'Else
            'MsgBox("ไม่พบข้อมูล", MsgBoxStyle.OkOnly, "Error Information")
            'End If
            Cmb_Line.ResetText()
            Lb_Status.Text = "Ready"
            Lb_Status.BackColor = Color.YellowGreen
            Lb_Status.Refresh()
            'Lb_Rec_Count.Text = "Total " & diff_dt.Rows.Count & " Records"
            Lb_Rec_Count.Text = "Total 0 Records"
        Catch ex As Exception
            MsgBox(ex.ToString)
            Cmb_Line.ResetText()
            Lb_Status.Text = "Ready"
            Lb_Status.BackColor = Color.YellowGreen
            Lb_Status.Refresh()
        End Try
    End Sub

    Private Sub Btt_Export_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btt_Export.Click
        Dim pdline As String = Cmb_Line.Text
        Dim dg_stk_dt As New DataTable
        Dim dg_tag_dt As New DataTable
        Dim dg_diff_dt As New DataTable
        Dim wiptagcls As New wipTAG

        Select Case button_selection
            Case Is = "STK_List"
                Lb_Status.Text = "Export Stock List to Excel"
                Lb_Status.BackColor = Color.Tomato
                dg_stk_dt = DG_View.DataSource
                pdline = DG_View.Item("pdline", 1).Value.ToString
                'wiptagcls.Excel_Get_list(pdline, dg_stk_dt)
                MsgBox("Export Stock List Finished")
                'Case Is = "Prt_Tag"
                '    dg_tag_dt = DG_View.DataSource
                '    wiptagcls.Excel_Get_Tag(pdline, dg_tag_dt)
            Case Is = "Check_Diff"
                Lb_Status.Text = "Export Diff List to Excel"
                Lb_Status.BackColor = Color.LightPink
                dg_diff_dt = DG_View.DataSource
                wiptagcls.Excel_Get_Diff(dg_diff_dt, "Diff")
                MsgBox("Export Diff List Finished")
        End Select
        Lb_Status.Text = "Ready"
        Lb_Status.BackColor = Color.YellowGreen
    End Sub

    Private Sub Btt_Update_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btt_Update.Click
        Dim wiptagcls As New wipTAG

        Lb_Status.Text = "Upload Data"
        Lb_Status.BackColor = Color.LightPink
        Lb_Status.Refresh()
        'wiptagcls.Insert_Act_Qty()
        Lb_Status.Text = "Ready"
        Lb_Status.BackColor = Color.YellowGreen
        Lb_Status.Refresh()
    End Sub

    Private Sub Cmb_factory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cmb_factory.SelectedIndexChanged
        Dim ds As New wipTAGDataSet
        Dim dt_location As wipTAGDataSet.wip_inventory_locationDataTable
        Dim ad_location As New wipTAGDataSetTableAdapters.wip_inventory_locationTableAdapter
        Dim faccode As String = Cmb_factory.Text.ToString
        
        dt_location = ds.wip_inventory_location
        ad_location.FillBy(dt_location, faccode)

        'With Cmb_Line
        '.DataSource = dt_location
        '.DisplayMember = "loc_name"
        '.ValueMember = "loc_code"
        '.Text = "ALL"
        'End With

        Dim dr_location As DataRow
        dr_location = dt_location.NewRow
        dr_location(0) = 0
        dr_location(1) = "ALL"
        dt_location.Rows.InsertAt(dr_location, 0)

        With Cmb_Line
            .DataSource = dt_location
            .DisplayMember = "loc_name"
            .ValueMember = "loc_code"
            .Text = "ALL"
        End With

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        If FolderBrowserDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then
            Txb_Save_To.Text = FolderBrowserDialog1.SelectedPath
        End If
    End Sub
End Class