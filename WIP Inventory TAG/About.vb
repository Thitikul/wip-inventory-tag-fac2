﻿Public NotInheritable Class About

    Private Sub About_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' Set the title of the form.
        Dim ApplicationTitle As String
        If My.Application.Info.Title <> "" Then
            ApplicationTitle = My.Application.Info.Title
        Else
            ApplicationTitle = System.IO.Path.GetFileNameWithoutExtension(My.Application.Info.AssemblyName)
        End If
        Me.Text = String.Format("About {0}", ApplicationTitle)
        ' Initialize all of the text displayed on the About Box.
        ' TODO: Customize the application's assembly information in the "Application" pane of the project 
        '    properties dialog (under the "Project" menu).
        Me.LabelProductName.Text = My.Application.Info.ProductName
        Me.LabelProductName.Text = "WIP Inventory TAG"
        Me.LabelVersion.Text = String.Format("Version 1.3.0", My.Application.Info.Version.ToString)
        Me.LabelCopyright.Text = "Program by : Angsuchawal.K  " & My.Application.Info.Copyright
        Me.LabelCompanyName.Text = My.Application.Info.CompanyName
        Me.LabelCompanyName.Text = "NITTO MATEX (THAILAND) CO.,LTD."
        Me.TextBoxDescription.Text = My.Application.Info.Description
        Me.TextBoxDescription.Text = "           โปรแกรมนี้ได้พัฒนาขึ้นมาเพื่อปรับปรุงกระบวนการ การนับ Inventory ให้มีความถูกต้องของข้อมูลมากขึ้น และเสริมฟังก์ชั่นในการเปรียบเทียบข้อมูลหลังการปรับยอดในระบบ Prones ให้ตรงกับยอดนับจริง"
    End Sub

    Private Sub OKButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OKButton.Click
        Me.Close()
    End Sub

End Class
