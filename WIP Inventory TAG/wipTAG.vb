﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports WIP_Inventory_TAG



Public Class wipTAG
    Public tagADPT As New wipTAGDataSetTableAdapters.wip_inventory_tagTableAdapter
    Public tagDT As New wipTAGDataSet.wip_inventory_tagDataTable
    
    Public diffADPT As New wipTAGDataSetTableAdapters.wip_inventory_tagTableAdapter
    Public diffDT As New wipTAGDataSet.wip_inventory_tagDataTable

    Public ad_stock As New wipTAGDataSetTableAdapters.wip_inventory_stockTableAdapter
    Public dt_stock As New wipTAGDataSet.wip_inventory_stockDataTable

    Public file_name As String
    Public saveto As String

    Private Sub releaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub

    Public Function Excel_Get_list_before(ByVal faccode As String, ByVal loccode As String, ByVal dt_stock As System.Data.DataTable) As Boolean
        Dim stk_month, time_start, time_end As String
        Dim line_index, tag_id, row_index As Integer

        'MsgBox("EXCEL GET LIST Function is 'ON'")
        time_start = Format(Now(), "yyyyMMdd_hhmm")

        Dim excelapp As Microsoft.Office.Interop.Excel.Application
        Dim excel_wkb As Microsoft.Office.Interop.Excel.Workbook
        Dim excel_wsh As Microsoft.Office.Interop.Excel.Worksheet
        Dim misValue As Object = System.Reflection.Missing.Value

        excelapp = New Microsoft.Office.Interop.Excel.ApplicationClass
        excel_wkb = excelapp.Workbooks.Add(misValue)
        excel_wsh = excel_wkb.ActiveSheet
        'excelapp.Visible = True

        stk_month = Form1.stk_month

        excel_wsh.Cells.Font.Name = "Calibri"
        excel_wsh.Cells.Font.Size = 10

        excel_wsh.PageSetup.Orientation = Microsoft.Office.Interop.Excel.XlPageOrientation.xlLandscape
        excel_wsh.PageSetup.LeftMargin = 0.5
        excel_wsh.PageSetup.RightMargin = 0.5
        excel_wsh.PageSetup.TopMargin = 0.5
        excel_wsh.PageSetup.BottomMargin = 0.5
        excel_wsh.PageSetup.HeaderMargin = 0.5
        excel_wsh.PageSetup.FooterMargin = 0.5

        '== Excel Header =='
        line_index = 0
        excel_wsh.Cells(line_index + 1, 1) = "PRE-COUNT " & stk_month
        excel_wsh.Range("A1").Font.Size = 15
        line_index = 1
        excel_wsh.Cells(line_index + 1, 10) = "Count By : "
        excel_wsh.Cells(line_index + 1, 13) = "                                            "
        excel_wsh.Range("M" & line_index + 1).Font.Underline = True
        line_index = 2
        excel_wsh.Cells(line_index + 1, 10) = "Check By : "
        excel_wsh.Cells(line_index + 1, 13) = "                                            "
        excel_wsh.Range("M" & line_index + 1).Font.Underline = True
        line_index = 3
        excel_wsh.Cells(line_index + 1, 10) = "Approve By : "
        excel_wsh.Cells(line_index + 1, 13) = "                                            "
        excel_wsh.Range("M" & line_index + 1).Font.Underline = True
        line_index = 4
        excel_wsh.Cells(line_index + 1, 10) = "Audit By : "
        excel_wsh.Cells(line_index + 1, 13) = "                                            "
        excel_wsh.Range("M" & line_index + 1).Font.Underline = True

        excel_wsh.Range("J4").Columns.AutoFit()

        line_index = 5
        excel_wsh.Cells(line_index + 1, 1) = "FAC CODE"
        excel_wsh.Cells(line_index + 1, 2) = "ITEM CODE"
        excel_wsh.Range("B" & (line_index + 1)).Columns.ColumnWidth = 30
        excel_wsh.Cells(line_index + 1, 3) = "LOCATION"
        excel_wsh.Cells(line_index + 1, 4) = "WAREHOUSE ADDRESS"
        excel_wsh.Range("D" & (line_index + 1)).Columns.ColumnWidth = 20
        excel_wsh.Cells(line_index + 1, 5) = "LOT NO"
        excel_wsh.Range("E" & (line_index + 1)).Columns.ColumnWidth = 15
        excel_wsh.Cells(line_index + 1, 6) = "TAG NO"
        excel_wsh.Cells(line_index + 1, 7) = "OH QTY"
        excel_wsh.Cells(line_index + 1, 8) = "ACT QTY"
        excel_wsh.Cells(line_index + 1, 9) = "DIFF QTY"
        excel_wsh.Cells(line_index + 1, 10) = "UNIT DESC"
        excel_wsh.Cells(line_index + 1, 11) = "COUNT BY"
        excel_wsh.Cells(line_index + 1, 12) = "AUDIT BY"
        excel_wsh.Cells(line_index + 1, 13) = "REMARK"
        excel_wsh.Range("M" & (line_index + 1)).Columns.ColumnWidth = 20
        'excel_wsh.Cells(line_index + 1, 15) = "MONTH"
        excel_wsh.Range("A1", "M1").Font.Bold = True
        excel_wsh.Range("A6", "M6").Font.Bold = True

        With excel_wsh.Range("A6", "M6").Borders
            .LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlDot
            .Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin
        End With

        'Try
        '== Excel Detail =='
        For row_index = 0 To dt_stock.Rows.Count - 1
            If loccode = "ALL" Or loccode = dt_stock.Rows(row_index).Item("stocklocation").ToString() Then
                excel_wsh.Cells(line_index + 2, 1) = "'" & dt_stock.Rows(row_index).Item("faccd").ToString()
                excel_wsh.Range("A" & line_index + 2).RowHeight = 40
                excel_wsh.Cells(line_index + 2, 2) = "'" & dt_stock.Rows(row_index).Item("itemcd").ToString() & vbNewLine & dt_stock.Rows(row_index).Item("itemdesc").ToString()
                excel_wsh.Cells(line_index + 2, 3) = "'" & dt_stock.Rows(row_index).Item("stocklocation").ToString()
                excel_wsh.Cells(line_index + 2, 4) = "                                            "
                excel_wsh.Range("D" & line_index + 2).Font.Underline = True
                excel_wsh.Cells(line_index + 2, 5) = "'" & dt_stock.Rows(row_index).Item("lotno").ToString()
                tag_id = row_index + 1
                excel_wsh.Cells(line_index + 2, 6) = "'" & Format(tag_id, "0000")
                excel_wsh.Cells(line_index + 2, 7) = "" & dt_stock.Rows(row_index).Item("onhand").ToString()
                excel_wsh.Cells(line_index + 2, 8) = "                      "
                excel_wsh.Range("H" & line_index + 2).Font.Underline = True
                excel_wsh.Cells(line_index + 2, 9) = "=IF(ISNUMBER(H" & (line_index + 2) & ") = FALSE,H" & (line_index + 2) & ",H" & (line_index + 2) & "-G" & (line_index + 2) & ")"
                excel_wsh.Cells(line_index + 2, 10) = "'" & dt_stock.Rows(row_index).Item("unitdesc").ToString()
                excel_wsh.Cells(line_index + 2, 11) = "                      "
                excel_wsh.Range("K" & line_index + 2).Font.Underline = True
                excel_wsh.Cells(line_index + 2, 12) = "                      "
                excel_wsh.Range("L" & line_index + 2).Font.Underline = True
                excel_wsh.Cells(line_index + 2, 13) = "                                            "
                excel_wsh.Range("M" & line_index + 2).Font.Underline = True
                'excel_wsh.Cells(line_index + 2, 15) = "'" & stk_month

                With excel_wsh.Range("A" & (line_index + 2), "M" & (line_index + 2)).Borders
                    .LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlDot
                    .Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin
                End With

                line_index = line_index + 1

            End If
        Next
        'Catch ex As Exception
        'MsgBox(ex.ToString)

        'End Try
        '== Margin ========'
        excel_wsh.Range("F" & (line_index + 1)).Columns.Hidden = True
        excel_wsh.Range("K" & (line_index + 1)).Columns.Hidden = True
        excel_wsh.Range("L" & (line_index + 1)).Columns.Hidden = True

        dt_stock = Nothing

        saveto = Form1.Txb_Save_To.Text
        'file_name = saveto & "\WIP_Inventory_List_Before_" & loccode.Trim() & "_" & Format(Now(), "yyyyMMdd_hhmm")
        file_name = saveto & "\list_before_" & faccode.Trim() & "_" & loccode.Trim() & "_" & Format(Now(), "yyyyMMdd_hhmm")

        excel_wsh.SaveAs(file_name)
        excel_wkb.Close()
        excelapp.Quit()

        releaseObject(excelapp)
        releaseObject(excel_wkb)
        releaseObject(excel_wsh)

        time_end = Format(Now(), "yyyyMMdd_hhmm")
        'MsgBox("EXCEL GET LIST Function is finished")

        'MsgBox("start " & time_start & " end " & time_end)

    End Function

    Public Function Excel_Get_list_after(ByVal faccode As String, ByVal loccode As String, ByVal dt_stock As System.Data.DataTable) As Boolean
        Dim line_index, row_index, tag_id As Integer
        Dim stk_month, time_start, time_end As String

        'MsgBox("EXCEL GET LIST Function is 'ON'")
        time_start = Format(Now(), "yyyyMMdd_hhmm")

        Dim excelapp As Microsoft.Office.Interop.Excel.Application
        Dim excel_wkb As Microsoft.Office.Interop.Excel.Workbook
        Dim excel_wsh As Microsoft.Office.Interop.Excel.Worksheet
        Dim misValue As Object = System.Reflection.Missing.Value

        excelapp = New Microsoft.Office.Interop.Excel.ApplicationClass
        excel_wkb = excelapp.Workbooks.Add(misValue)
        excel_wsh = excel_wkb.ActiveSheet
        'excelapp.Visible = True

        stk_month = Form1.stk_month

        excel_wsh.Cells.Font.Name = "Calibri"
        excel_wsh.Cells.Font.Size = 10

        excel_wsh.PageSetup.Orientation = Microsoft.Office.Interop.Excel.XlPageOrientation.xlLandscape
        excel_wsh.PageSetup.LeftMargin = 0.5
        excel_wsh.PageSetup.RightMargin = 0.5
        excel_wsh.PageSetup.TopMargin = 0.5
        excel_wsh.PageSetup.BottomMargin = 0.5
        excel_wsh.PageSetup.HeaderMargin = 0.5
        excel_wsh.PageSetup.FooterMargin = 0.5



        'Try
        '== Excel Header =='
        line_index = 0
        excel_wsh.Cells(line_index + 1, 1) = "PHYSICAL COUNT " & stk_month
        excel_wsh.Range("A1").Font.Size = 15

        line_index = 1
        excel_wsh.Cells(line_index + 1, 10) = "Count By : "
        excel_wsh.Cells(line_index + 1, 13) = "                                            "
        excel_wsh.Range("M" & line_index + 1).Font.Underline = True
        line_index = 2
        excel_wsh.Cells(line_index + 1, 10) = "Check By : "
        excel_wsh.Cells(line_index + 1, 13) = "                                            "
        excel_wsh.Range("M" & line_index + 1).Font.Underline = True
        line_index = 3
        excel_wsh.Cells(line_index + 1, 10) = "Approve By : "
        excel_wsh.Cells(line_index + 1, 13) = "                                            "
        excel_wsh.Range("M" & line_index + 1).Font.Underline = True
        line_index = 4
        excel_wsh.Cells(line_index + 1, 10) = "Audit By : "
        excel_wsh.Cells(line_index + 1, 13) = "                                            "
        excel_wsh.Range("M" & line_index + 1).Font.Underline = True

        excel_wsh.Range("J4").Columns.AutoFit()

        line_index = 5

        excel_wsh.Cells(line_index + 1, 1) = "FAC CODE"
        excel_wsh.Cells(line_index + 1, 2) = "ITEM CODE"
        excel_wsh.Range("B" & (line_index + 1)).Columns.ColumnWidth = 35
        excel_wsh.Cells(line_index + 1, 3) = "LOCATION"
        excel_wsh.Cells(line_index + 1, 4) = "WAREHOUSE ADDRESS"
        excel_wsh.Range("D" & (line_index + 1)).Columns.ColumnWidth = 20
        excel_wsh.Cells(line_index + 1, 5) = "LOT NO"
        excel_wsh.Range("E" & (line_index + 1)).Columns.ColumnWidth = 15
        excel_wsh.Cells(line_index + 1, 6) = "TAG NO"
        excel_wsh.Cells(line_index + 1, 7) = "OH QTY"
        excel_wsh.Cells(line_index + 1, 8) = "ACT QTY"
        excel_wsh.Cells(line_index + 1, 9) = "DIFF QTY"
        excel_wsh.Cells(line_index + 1, 10) = "UNIT DESC"
        excel_wsh.Cells(line_index + 1, 11) = "COUNT BY"
        excel_wsh.Cells(line_index + 1, 12) = "AUDIT BY"
        excel_wsh.Cells(line_index + 1, 13) = "REMARK"
        excel_wsh.Range("M" & (line_index + 1)).Columns.ColumnWidth = 20
        'excel_wsh.Cells(line_index + 1, 15) = "MONTH"
        excel_wsh.Range("A1", "M1").Font.Bold = True
        excel_wsh.Cells(line_index + 1, 14) = "ITEM_CD-LOT_NO"

        With excel_wsh.Range("A6", "M6").Borders
            .LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlDot
            .Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin
        End With
        excel_wsh.Range("A6", "M6").Font.Bold = True

        '== Excel Detail =='
        For row_index = 0 To dt_stock.Rows.Count - 1
            If loccode = "ALL" Or loccode = dt_stock.Rows(row_index).Item("stocklocation").ToString() Then
                excel_wsh.Cells(line_index + 2, 1) = "'" & dt_stock.Rows(row_index).Item("faccd").ToString()
                excel_wsh.Range("A" & line_index + 2).RowHeight = 40
                excel_wsh.Cells(line_index + 2, 2) = "'" & dt_stock.Rows(row_index).Item("itemcd").ToString() & vbNewLine & dt_stock.Rows(row_index).Item("itemdesc").ToString()
                excel_wsh.Cells(line_index + 2, 3) = "'" & dt_stock.Rows(row_index).Item("stocklocation").ToString()
                excel_wsh.Cells(line_index + 2, 4) = "                                            "
                excel_wsh.Range("D" & line_index + 2).Font.Underline = True
                excel_wsh.Cells(line_index + 2, 5) = "'" & dt_stock.Rows(row_index).Item("lotno").ToString()
                tag_id = row_index + 1
                excel_wsh.Cells(line_index + 2, 6) = "'" & Format(tag_id, "0000")
                excel_wsh.Cells(line_index + 2, 7) = "" & dt_stock.Rows(row_index).Item("onhand").ToString()
                excel_wsh.Cells(line_index + 2, 8) = "" & dt_stock.Rows(row_index).Item("onhand").ToString()
                excel_wsh.Cells(line_index + 2, 9) = "=IF(ISNUMBER(H" & (line_index + 2) & ") = FALSE,H" & (line_index + 2) & ",G" & (line_index + 2) & "-H" & (line_index + 2) & ")"
                excel_wsh.Cells(line_index + 2, 10) = "'" & dt_stock.Rows(row_index).Item("unitdesc").ToString()
                excel_wsh.Cells(line_index + 2, 11) = "                      "
                excel_wsh.Range("K" & line_index + 2).Font.Underline = True
                excel_wsh.Cells(line_index + 2, 12) = "                      "
                excel_wsh.Range("L" & line_index + 2).Font.Underline = True
                excel_wsh.Cells(line_index + 2, 13) = "                                            "
                excel_wsh.Range("M" & line_index + 2).Font.Underline = True
                'excel_wsh.Cells(line_index + 2, 15) = "'" & stk_month
                excel_wsh.Cells(line_index + 2, 14) = "'" & dt_stock.Rows(row_index).Item("itemcd").ToString().Trim()
                excel_wsh.Cells(line_index + 2, 15) = dt_stock.Rows(row_index).Item("lotno").ToString().Trim()

                With excel_wsh.Range("A" & (line_index + 2), "M" & (line_index + 2)).Borders
                    .LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlDot
                    .Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin
                End With

                line_index = line_index + 1
            End If
        Next
        'Catch ex As Exception
        'MsgBox(ex.ToString)

        'End Try
        '== Margin ========'
        excel_wsh.Range("D" & (line_index + 1)).Columns.Hidden = True
        excel_wsh.Range("G" & (line_index + 1)).Columns.Hidden = True
        excel_wsh.Range("I" & (line_index + 1)).Columns.Hidden = True
        excel_wsh.Range("K" & (line_index + 1)).Columns.Hidden = True
        excel_wsh.Range("L" & (line_index + 1)).Columns.Hidden = True

        dt_stock = Nothing

        saveto = Form1.Txb_Save_To.Text
        file_name = saveto & "\list_after_" & faccode.Trim() & "_" & loccode.Trim() & "_" & Format(Now(), "yyyyMMdd_hhmm")

        excel_wsh.SaveAs(file_name)
        excel_wkb.Close()
        excelapp.Quit()

        releaseObject(excelapp)
        releaseObject(excel_wkb)
        releaseObject(excel_wsh)

        time_end = Format(Now(), "yyyyMMdd_hhmm")
        'MsgBox("EXCEL GET LIST Function is finished")

        'MsgBox("start " & time_start & " end " & time_end)

    End Function

    Public Function Excel_Get_Tag(ByVal faccode As String, ByVal loccode As String, ByVal dt_tag As System.Data.DataTable) As Boolean

        Dim stk_month, time_start, time_end As String
        Dim line_index, line_sp, row_index, column_sp As Integer
        Dim page_sp, page_grp, tag_id As Integer

        'MsgBox("EXCEL GET TAG Function is 'ON'")
        time_start = Format(Now(), "yyyyMMdd_hhmm")

        Dim excel_app2 As Microsoft.Office.Interop.Excel.Application
        Dim excel_wkb2 As Microsoft.Office.Interop.Excel.Workbook
        Dim excel_wsh2 As Microsoft.Office.Interop.Excel.Worksheet
        Dim misValue As Object = System.Reflection.Missing.Value

        excel_app2 = New Microsoft.Office.Interop.Excel.ApplicationClass
        excel_wkb2 = excel_app2.Workbooks.Add(misValue)
        excel_wsh2 = excel_wkb2.ActiveSheet
        'excel_app2.Visible = True

        excel_wsh2.PageSetup.Orientation = Microsoft.Office.Interop.Excel.XlPageOrientation.xlPortrait

        excel_wsh2.PageSetup.LeftMargin = 0.0
        excel_wsh2.PageSetup.RightMargin = 0.0
        excel_wsh2.PageSetup.TopMargin = 0.0
        excel_wsh2.PageSetup.BottomMargin = 0.0
        excel_wsh2.PageSetup.HeaderMargin = 0.0
        excel_wsh2.PageSetup.FooterMargin = 0.0
        'excel_wsh2.PageSetup.CenterHorizontally = True
        'excel_wsh2.PageSetup.CenterVertically = True

        excel_wsh2.PageSetup.PaperSize = Microsoft.Office.Interop.Excel.XlPaperSize.xlPaperA4
        'excel_wsh2.PageSetup.Zoom = False
        'excel_wsh2.PageSetup.FitToPagesWide = 1

        stk_month = Form1.stk_month
        'stk_month = "201601"

        excel_wsh2.Cells.Font.Name = "Calibri"
        excel_wsh2.Cells.Font.Size = 11
        'excel_wsh2.Cells.Font.Bold = True

        line_index = 0
        line_sp = 0
        column_sp = 0
        page_grp = 27 '24
        tag_id = 1
        row_index = 0
        page_sp = 0
        For line_sp = 0 To dt_tag.Rows.Count - 1

            If loccode = "ALL" Or loccode = dt_tag.Rows(row_index).Item("stocklocation").ToString() Then

                excel_wsh2.Cells(7 + (line_index * page_grp), 2 + (page_sp * 8)) = "Original"
                excel_wsh2.Range("B" & (7 + (line_index * page_grp))).Font.Size = 20
                excel_wsh2.Range("B" & (7 + (line_index * page_grp))).Font.Bold = True
                
                excel_wsh2.Cells(9 + (line_index * page_grp), 2 + (page_sp * 8)) = "Fac Code : "
                excel_wsh2.Cells(9 + (line_index * page_grp), 3 + (page_sp * 8)) = "'" & dt_tag.Rows(row_index).Item("faccd").ToString()

                excel_wsh2.Cells(9 + (line_index * page_grp), 5 + (page_sp * 8)) = "Location : "
                excel_wsh2.Cells(9 + (line_index * page_grp), 6 + (page_sp * 8)) = "'" & dt_tag.Rows(row_index).Item("stocklocation").ToString()

                excel_wsh2.Cells(9 + (line_index * page_grp), 8 + (page_sp * 8)) = "Warehouse Address"
                excel_wsh2.Cells(10 + (line_index * page_grp), 8 + (page_sp * 8)) = "'1.                                     "
                excel_wsh2.Cells(11 + (line_index * page_grp), 8 + (page_sp * 8)) = "'2.                                     "
                excel_wsh2.Cells(12 + (line_index * page_grp), 8 + (page_sp * 8)) = "'3.                                     "
                excel_wsh2.Cells(13 + (line_index * page_grp), 8 + (page_sp * 8)) = "'4.                                     "
                excel_wsh2.Cells(14 + (line_index * page_grp), 8 + (page_sp * 8)) = "'5.                                     "

                excel_wsh2.Range("H" & (10 + (line_index * page_grp))).Font.Underline = True
                excel_wsh2.Range("H" & (11 + (line_index * page_grp))).Font.Underline = True
                excel_wsh2.Range("H" & (12 + (line_index * page_grp))).Font.Underline = True
                excel_wsh2.Range("H" & (13 + (line_index * page_grp))).Font.Underline = True
                excel_wsh2.Range("H" & (14 + (line_index * page_grp))).Font.Underline = True
                
                excel_wsh2.Cells(11 + (line_index * page_grp), 2 + (page_sp * 8)) = "Lot No : "
                excel_wsh2.Cells(11 + (line_index * page_grp), 3 + (page_sp * 8)) = "'" & dt_tag.Rows(row_index).Item("lotno").ToString()

                excel_wsh2.Cells(13 + (line_index * page_grp), 2 + (page_sp * 8)) = "Item Code : "
                excel_wsh2.Cells(13 + (line_index * page_grp), 3 + (page_sp * 8)) = "'" & dt_tag.Rows(row_index).Item("itemcd").ToString()

                excel_wsh2.Cells(15 + (line_index * page_grp), 2 + (page_sp * 8)) = "Item Desc : "
                excel_wsh2.Cells(15 + (line_index * page_grp), 3 + (page_sp * 8)) = "'" & dt_tag.Rows(row_index).Item("itemdesc").ToString()

                excel_wsh2.Cells(17 + (line_index * page_grp), 2 + (page_sp * 8)) = "Qty : "
                excel_wsh2.Cells(17 + (line_index * page_grp), 3 + (page_sp * 8)) = "                                        "
                excel_wsh2.Cells(17 + (line_index * page_grp), 5 + (page_sp * 8)) = "'" & dt_tag.Rows(row_index).Item("unitdesc").ToString()
                excel_wsh2.Cells(17 + (line_index * page_grp), 7 + (page_sp * 8)) = "Count By : "
                excel_wsh2.Cells(17 + (line_index * page_grp), 8 + (page_sp * 8)) = "                                        "

                excel_wsh2.Range("C" & (17 + (line_index * page_grp))).Font.Underline = True
                excel_wsh2.Range("H" & (17 + (line_index * page_grp))).Font.Underline = True
                
                excel_wsh2.Cells(19 + (line_index * page_grp), 2 + (page_sp * 8)) = "Check By : "
                excel_wsh2.Cells(19 + (line_index * page_grp), 3 + (page_sp * 8)) = "                                        "
                excel_wsh2.Cells(19 + (line_index * page_grp), 7 + (page_sp * 8)) = "Audit By : "
                excel_wsh2.Cells(19 + (line_index * page_grp), 8 + (page_sp * 8)) = "                                        "

                excel_wsh2.Range("C" & (19 + (line_index * page_grp))).Font.Underline = True
                excel_wsh2.Range("H" & (19 + (line_index * page_grp))).Font.Underline = True
                
                excel_wsh2.Cells(21 + (line_index * page_grp), 2 + (page_sp * 8)) = "'" & Left(stk_month, 4) & "/" & Right(stk_month, 2)
                tag_id = row_index + 1
                excel_wsh2.Cells(21 + (line_index * page_grp), 7 + (page_sp * 8)) = "'Tag No : "
                excel_wsh2.Cells(21 + (line_index * page_grp), 8 + (page_sp * 8)) = "'" & Format(tag_id, "0000")

                line_index = line_index + 1

            End If

            row_index = row_index + 1


        Next

        'excel_wsh2.Range("H1").Columns.ColumnWidth = excel_wsh2.Range("H1").Columns.ColumnWidth - 2
        'excel_wsh2.Range("C1").Columns.ColumnWidth = excel_wsh2.Range("C1").Columns.ColumnWidth - 4
        'excel_wsh2.Range("H1").Columns.ColumnWidth = excel_wsh2.Range("H1").Columns.ColumnWidth + 4
        'excel_wsh2.Range("K1").Columns.ColumnWidth = excel_wsh2.Range("K1").Columns.ColumnWidth - 4

        'excel_wsh2.Range("G1").Columns.ColumnWidth = excel_wsh2.Range("G1").Columns.ColumnWidth + 1
        'excel_wsh2.Range("O1").Columns.ColumnWidth = excel_wsh2.Range("O1").Columns.ColumnWidth + 1
        excel_wsh2.Range("B13").Columns.AutoFit()
        excel_wsh2.Range("G17").Columns.AutoFit()
        
        dt_tag = Nothing
        '== Margin ========'

        saveto = Form1.Txb_Save_To.Text
        file_name = saveto & "\wip_tag_" & faccode.Trim() & "_" & loccode.Trim() & "_" & Format(Now(), "yyyyMMdd_hhmm")

        excel_wsh2.SaveAs(file_name)
        excel_wkb2.Close()
        excel_app2.Quit()

        releaseObject(excel_app2)
        releaseObject(excel_wkb2)
        releaseObject(excel_wsh2)

        time_end = Format(Now(), "yyyyMMdd_hhmm")
        'MsgBox("EXCEL GET TAG Function is finished")

        'MsgBox("start " & time_start & " end " & time_end)

    End Function

    Public Function Get_Upload_Excel(ByVal file_select As String) As Boolean
        Dim myconnec As OleDbConnection
        Dim dtst As DataSet
        Dim mycomm As OleDbDataAdapter

        myconnec = New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source='" + file_select + "';Extended Properties=Excel 12.0;")
        'mycomm = New OleDbDataAdapter("Select * FROM [Sheet1$]", myconnec)
        Dim sqlcommand As String
        sqlcommand = ""
        sqlcommand = sqlcommand & "SELECT [FAC CODE] AS faccd"
        sqlcommand = sqlcommand & ", [ITEM CODE] AS itemcd"
        sqlcommand = sqlcommand & ", [LOC CODE] AS stocklocation"
        sqlcommand = sqlcommand & ", [LOT NO] AS lotno"
        sqlcommand = sqlcommand & ", [TAG NO] AS tagid"
        sqlcommand = sqlcommand & ", [OH QTY] AS onhand"
        sqlcommand = sqlcommand & ", [ACT QTY] AS actualcount"
        sqlcommand = sqlcommand & ", [DIFF QTY] AS diffqty"
        sqlcommand = sqlcommand & " FROM [Sheet1$]"
        mycomm = New OleDbDataAdapter(sqlcommand, myconnec)
        dtst = New DataSet()
        mycomm.Fill(dtst)
        MsgBox(dtst.Tables(0).Rows.Count.ToString)
        Form1.DG_View.AutoGenerateColumns = False
        Form1.DG_View.DataSource = dtst.Tables(0)
        myconnec.Close()


    End Function

    Public Function Insert_Act_Qty(ByVal dt_tag As System.Data.DataTable) As Boolean

    End Function

    'Public Function Get_Diff_Data() As wipTAGDataSet.diff_tableDataTable
    '    dfadpt.FillBy_Check_Diff_All(dfdt)
    '    Return dfdt
    'End Function

    Public Function Excel_Get_Diff(ByVal difflist As System.Data.DataTable, ByVal ct As String) As Boolean
        Dim diff_view As New DataView

        MsgBox("EXCEL GET DIFF Function is 'ON'")

        Dim excelapp As Microsoft.Office.Interop.Excel.Application
        Dim excel_wkb As Microsoft.Office.Interop.Excel.Workbook
        Dim excel_wsh As Microsoft.Office.Interop.Excel.Worksheet

        excel_wkb = excelapp.Workbooks.Add(1)
        excel_wsh = excel_wkb.ActiveSheet

        '== Excel Header =='


        diff_view.Table = difflist
        '== Excel Detail =='


        saveto = Form1.Txb_Save_To.Text
        file_name = saveto & "\WIP_Inventory_DIFF_List_" & Format(Now(), "yyyyMMdd_hhmm")
        excel_wkb.SaveAs(file_name)
        excelapp.Workbooks.Close()
        releaseObject(excelapp)
        releaseObject(excel_wkb)
        releaseObject(excel_wsh)
        MsgBox("Finished")
    End Function

    'Public Function Tag_To_DB(ByVal stk_dt As wipTAGDataSet.stk_dataDataTable) As Boolean
    'Dim stkdtv As New DataView
    'Dim t_itemcd, t_stocklocation, t_lotno, t_pdline, t_onhand, t_wos, t_seiban, t_partno, t_tagid As String
    'Dim t_actualcount, t_diffqty As Decimal

    '    stkdtv.Table = stk_dt
    '    For rw = 0 To stk_dt.Rows.Count - 1

    '        tagADPT.Tag_to_DB(tagDT, t_itemcd, t_stocklocation, t_lotno, t_pdline, CDec(t_onhand), t_wos, t_seiban, t_partno, t_tagid, CDec(t_actualcount), CDec(t_diffqty))
    '    Next
    'End Function

    'Public Function Tag_To_DB(ByVal stk_dt As wipTAGDataSet.stk_dataDataTable) As Boolean
    'Dim stkdtv As New DataView
    'Dim t_itemcd, t_stocklocation, t_lotno, t_pdline, t_onhand, t_wos, t_seiban, t_partno, t_tagid As String
    'Dim t_actualcount, t_diffqty As Decimal

    '    stkdtv.Table = stk_dt
    '    For rw = 0 To stk_dt.Rows.Count - 1

    '        tagADPT.Tag_to_DB(tagDT, t_itemcd, t_stocklocation, t_lotno, t_pdline, CDec(t_onhand), t_wos, t_seiban, t_partno, t_tagid, CDec(t_actualcount), CDec(t_diffqty))
    '    Next
    'End Function

    Public Function Get_Tag_Data(ByVal pdline As String) As wipTAGDataSet.wip_inventory_tagDataTable
        tagADPT.Fill_Tag_Data(tagDT, pdline)
        Return tagDT
    End Function

    Public Function Get_StkTag_Data(ByVal pdline As String) As wipTAGDataSet.wip_inventory_tagDataTable
        tagADPT.Fill_Tag_Data(tagDT, pdline)
        tagADPT.Fill_StkTag_Data(tagDT, pdline)
        Return tagDT
    End Function

    Public Function Clear_Tag_Data(ByVal pdline As String) As Boolean
        tagADPT.Clear_Tag(tagDT, pdline)
    End Function

    Public Function Clear_DB() As Boolean
        'tag_tmp_adpt.Clear_DB()
    End Function

    Public Function Import_to_DB() As Boolean

    End Function


    Public Function Gen_Tag_ID(ByVal loccode As String, ByVal stkdata As wipTAGDataSet.wip_inventory_stockDataTable) As wipTAGDataSet.wip_inventory_stockDataTable
        Dim stkdtv As New DataView
        Dim tagcount As Long = 0

        stkdtv.Table = stkdata
        For rw = 0 To stkdata.Rows.Count - 1

            If stkdtv.Table(rw)("onhand") > 0 Then
                tagcount = tagcount + 1
                If loccode = "OTHER" Then
                    stkdtv.Table(rw)("tagid") = "OTH" & Format(tagcount, "000")
                Else
                    stkdtv.Table(rw)("tagid") = loccode & Format(tagcount, "000")
                End If

            End If
            MsgBox(stkdtv.Table(rw)("onhand") & Chr(10) & stkdtv.Table(rw)("tagid"))
        Next
        Return stkdtv.Table
    End Function

End Class
